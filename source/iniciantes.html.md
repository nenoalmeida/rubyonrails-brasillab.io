---
title: Iniciantes
layout: page
---
Se voce é iniciante veja nossas [pesquisas oficiais](/2016/04/26/pesquisas-oficiais),
onde a comunidade opina sobre as melhores práticas e o que eles estão usando
para desenvolver Rails.

O Fabio Akita ([@AkitaOnRails](https://twitter.com/AkitaOnRails)) escreveu um
[artigo com algumas dicas](http://www.akitaonrails.com/2015/07/20/small-bites-comecando-no-grupo-ruby-on-rails-brasil-no-facebook)
pra quem está começando com Ruby on Rails e entrou na nossa comunidade no
Facebook.

Existem duas listas curadas pela comunidade internacional de Gems para a mais
variada gama de problemas:

* [Awesome Ruby](http://awesome-ruby.com/)
* [Awesome Rails](https://github.com/ekremkaraca/awesome-rails)
